using Grpc.Core;
using Retr0.GrpcBenchmarks.DataAccess;

namespace Retr0.GrpcBenchmarks.GrpcService.Services
{
    public class UserService : GrpcUserService.GrpcUserServiceBase
    {
        private readonly ILogger<UserService> _logger;
        private readonly UserRepository _userRepository;

        public UserService(ILogger<UserService> logger, UserRepository userRepository)
        {
            _logger = logger;
            _userRepository = userRepository;
        }

        public async override Task<GrpcUsersReply> GetUsers(GrpcEmptyRequest _, ServerCallContext context)
        {
            IEnumerable<User> users = await _userRepository.GetUsers();

            GrpcUsersReply reply = new GrpcUsersReply();
            reply.Users.AddRange(users.Select(s => new GrpcUser
            {
                Id = s.Id.ToString(),
                Name = s.Name
            }));

            return reply;
        }
    }
}