﻿using System.Text.Json;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

using Grpc.Net.Client;

using Retr0.GrpcBenchmarks.DataAccess;

namespace Retr0.GrpcBenchmarks.Main
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            Console.WriteLine("Press ENTER after API and gRPC services have started to begin benchmarking.");
            Console.ReadLine();

            var summary = BenchmarkRunner.Run(typeof(Program).Assembly);
        }
    }

    [CsvExporter]
    [HtmlExporter]
    public class GrpcBenchmarks
    {
        private readonly UserRepository _userRepository;
        private readonly HttpClient _httpClient;

        private readonly GrpcChannel _localChannel;
        private readonly GrpcUserService.GrpcUserServiceClient _localGrpcClient;

        private readonly GrpcChannel _localDockerChannel;
        private readonly GrpcUserService.GrpcUserServiceClient _localDockerGrpcClient;

        private readonly GrpcChannel _azureChannel;
        private readonly GrpcUserService.GrpcUserServiceClient _azureGrpcClient;

        public GrpcBenchmarks()
        {
            _userRepository = new UserRepository();

            _httpClient = new HttpClient();

            _localChannel = GrpcChannel.ForAddress("https://localhost:7132");
            _localGrpcClient = new GrpcUserService.GrpcUserServiceClient(_localChannel);

            _localDockerChannel = GrpcChannel.ForAddress("http://localhost:5000");
            _localDockerGrpcClient = new GrpcUserService.GrpcUserServiceClient(_localDockerChannel);

            _azureChannel = GrpcChannel.ForAddress("https://ca-dpbtestgrpc-dev-westeurope-01.icyriver-74f8f169.westeurope.azurecontainerapps.io");
            _azureGrpcClient = new GrpcUserService.GrpcUserServiceClient(_azureChannel);
        }

        [Benchmark(Baseline = true)]
        public async Task GetUsersDirect()
        {
            await _userRepository.GetUsers();
        }

        [Benchmark]
        public async Task GetUsersViaHttp()
        {
            string url = "https://localhost:7129/users";

            HttpResponseMessage response = await _httpClient.GetAsync(url);
            string responseString = await response.Content.ReadAsStringAsync();

            IEnumerable<User> users = JsonSerializer.Deserialize<IEnumerable<User>>(responseString);
        }

        [Benchmark]
        public async Task GetUsersViaHttpAzure()
        {
            string url = "https://app-dpbtestapi-dev-westeurope-001.azurewebsites.net/users";

            HttpResponseMessage response = await _httpClient.GetAsync(url);
            string responseString = await response.Content.ReadAsStringAsync();

            IEnumerable<User> users = JsonSerializer.Deserialize<IEnumerable<User>>(responseString);
        }

        [Benchmark]
        public async Task GetUsersViaGrpc()
        {
            var reply = await _localGrpcClient.GetUsersAsync(new GrpcEmptyRequest());
            IEnumerable<User> users = reply.Users.Select(s => new User
            {
                Id = Guid.Parse(s.Id),
                Name = s.Name
            });
        }

        [Benchmark]
        public async Task GetUsersViaGrpcLocalDocker()
        {
            var reply = await _localDockerGrpcClient.GetUsersAsync(new GrpcEmptyRequest());
            IEnumerable<User> users = reply.Users.Select(s => new User
            {
                Id = Guid.Parse(s.Id),
                Name = s.Name
            });
        }

        [Benchmark]
        public async Task GetUsersViaGrpcAzure()
        {
            var reply = await _azureGrpcClient.GetUsersAsync(new GrpcEmptyRequest());
            IEnumerable<User> users = reply.Users.Select(s => new User
            {
                Id = Guid.Parse(s.Id),
                Name = s.Name
            });
        }
    }
}