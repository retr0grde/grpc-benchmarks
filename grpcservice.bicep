// Specifying scope of deployment for this Bicep file
targetScope = 'resourceGroup'

param location string = resourceGroup().location
param serviceName string = 'dpbtestgrpc'

param containerImage string = 'retr0grde/grpcservice:0.0.3'
param containerPort int = 5000

resource caeLaw 'Microsoft.OperationalInsights/workspaces@2020-03-01-preview' = {
  name: 'log-${serviceName}-dev-${location}'
  location: location
  properties: any({
    retentionInDays: 30
    features: {
      searchVersion: 1
    }
    sku: {
      name: 'PerGB2018'
    }
  })
}

resource containerAppEnv 'Microsoft.App/managedEnvironments@2022-03-01' = {
  name: 'caenv-${serviceName}-dev-${location}-01'
  location: location
  properties: {
    appLogsConfiguration: {
      destination: 'log-analytics'
      logAnalyticsConfiguration: {
        customerId: caeLaw.properties.customerId
        sharedKey: caeLaw.listKeys().primarySharedKey
      }
    }
  }
}

resource containerApp 'Microsoft.App/containerApps@2022-03-01' = {
  name: 'ca-${serviceName}-dev-${location}-01'
  location: location
  properties: {
    managedEnvironmentId: containerAppEnv.id
    configuration: {  
      ingress: {
        external: true
        targetPort: containerPort
        transport: 'http2'
      }
    }
    template: {
      containers: [
        {
          image: containerImage
          name: 'grpcservice'
          env: [
            {
            name: 'ASPNETCORE_ENVIRONMENT'
            value: 'Production'
            }
          ]
        }
      ]
      scale: {
        minReplicas: 1
      }
    }
  }
}

output fqdn string = containerApp.properties.configuration.ingress.fqdn
