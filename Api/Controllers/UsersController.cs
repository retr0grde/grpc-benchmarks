using Microsoft.AspNetCore.Mvc;
using Retr0.GrpcBenchmarks.DataAccess;

namespace Retr0.GrpcBenchmarks.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly UserRepository _userRepository;

        public UsersController(ILogger<UsersController> logger, UserRepository userRepository)
        {
            _logger = logger;
            _userRepository = userRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            return await _userRepository.GetUsers();
        }
    }
}