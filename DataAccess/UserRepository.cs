﻿using Faker;

namespace Retr0.GrpcBenchmarks.DataAccess
{
    public class UserRepository
    {
        private readonly IList<User> Users;

        public UserRepository()
        {
            Users = new List<User>();
            for (int i = 0; i < 100; i++)
            {
                Users.Add(new User()
                {
                    Id = Guid.NewGuid(),
                    Name = Name.FullName(),
                });
            }
        }

        public async Task<IList<User>> GetUsers()
        {
            await Task.Delay(50);
            return Users;
        }
    }
}