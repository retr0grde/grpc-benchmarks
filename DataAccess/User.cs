﻿using System.Text.Json.Serialization;

namespace Retr0.GrpcBenchmarks.DataAccess
{
    public class User
    {
        [JsonPropertyName("Id")]
        public Guid Id { get; set; }

        [JsonPropertyName("Name")]
        public string Name { get; set; }
    }
}